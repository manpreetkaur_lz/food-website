<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Food;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of Items
 *
 * @author pc8
 */
class Item extends Model
{
    //put your code here
    protected $fillable = ['title','description','price','category_id','featured'];
    
    public function category() {
        
        return $this->belongsTo('App\Models\Food\Category');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Models\Food\Order','order_items');
    }
}