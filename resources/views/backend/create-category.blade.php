@extends('backend.layouts.app')
@section('content')
{{ Form::open(['route'=>'admin.category.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Add New Category</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('name', 'Name',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="col-sm-offset-2 col-sm-8">
            {{ Form::submit('Create', ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
    <!-- /.box-body -->
</div>
@endsection