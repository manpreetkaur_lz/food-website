<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Models\Food;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of Ctaegory
 *
 * @author pc8
 */
class Category extends Model
{
    //put your code here
    protected $fillable = ['name'];
    
    public function items()
    {
        return $this->hasMany('App\Models\Food\Item');
    }

}