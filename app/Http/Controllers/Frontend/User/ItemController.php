<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Food\Item;
use App\Models\Food\Category;
use App\Models\Food\UserCart;

/**
 * Description of ItemsController
 *
 * @author pc8
 */
class ItemController extends Controller
{

    //put your code here
    public function index()
    {
        $items = Item::where('featured', 1)->get()->toArray();
        return view('frontend.user.item', compact('items'));
    }

    public function search(Request $request)
    {
        $search = $request['search'];
        $books  = Book::where('title', 'LIKE', '%'.$search.'%')->orWhere('price',
                'LIKE', '%'.$search.'%')->orderBy('title')->get();
        return view('frontend.user.bookDashboard', ['books' => $books]);
    }

    public function getItemsOrCategories(Request $request)
    {
        $items = Item::where('title', 'like', '%'.$request->search.'%')->where('featured',
                1)->get()->toArray();
        if (empty($items)) {
            $item_data = [];
            $category  = Category::where('name', 'like',
                    '%'.$request->search.'%')->with('items')->get()->toArray();
            if (!empty($category)) {
                foreach ($category as $data) {
                    array_push($item_data, $data['items']);
                }
                $items = $item_data[0];
                return view('frontend.user.item', compact('items'));
            }
        }
        return view('frontend.user.item', compact('items'));
    }

    public function addCart(Request $request)
    {
        $cart_data = UserCart::where('user_id', access()->id())->first();
        if (!empty($cart_data)) {
            $items = json_decode($cart_data['items'], TRUE);
            if (in_array($request->item_id, $items['item_id'])) {
                return response()->json('exist', 200);
            } else {
                $items['item_id'][] = $request->item_id;
                $cart_data->items   = json_encode($items);
                if ($cart_data->save()) {
                    return response()->json('success', 200);
                }
            }
        } else {
            $cart              = new UserCart();
            $cart->user_id     = access()->id();
            $item              = [];
            $item['item_id'][] = $request->item_id;
            $cart->items       = json_encode($item);
            if ($cart->save()) {
                return response()->json('success', 200);
            }
        }
        return response()->json('Error', 500);
    }

    public function deleteCart($id = null)
    {
        $cart_data = UserCart::where('user_id', access()->id())->first();
        $items     = json_decode($cart_data['items'], TRUE);
        if ($id != null) {
            if (($key = array_search($id, $items['item_id'])) !== false) {
                unset($items['item_id'][$key]);
                $item2['item_id'] = array_values($items['item_id']);
                $cart_data->update(['items' => json_encode($item2)]);
                return redirect()->route('frontend.user.items.showCart')->withFlashSuccess('Cart Deleted Successfully.');
            }
        }
        return response()->json('Error', 500);
    }

    public function showCart()
    {
        $cart_data = UserCart::where('user_id', access()->id())->first();
        if (!empty($cart_data)) {
            $items = json_decode($cart_data['items'], TRUE);
            $data  = Item::whereIn('id', $items['item_id'])->with('category')->get()->toArray();
            return view('frontend.user.show_cart', compact('data'));
        }
        return view('frontend.user.show_cart');
    }
}