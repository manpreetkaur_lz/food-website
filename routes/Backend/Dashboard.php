<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
/*
 * Item routes
 */
Route::get('items', 'ItemsController@index')->name('items.index');
Route::get('create', 'ItemsController@create')->name('items.create');
Route::post('store', 'ItemsController@store')->name('items.store');
Route::get('getItem/{id}', 'ItemsController@getItem')->name('item.getItem');
Route::post('updateItem/{id}', 'ItemsController@update')->name('item.update');
Route::get('deleteItem/{id}', 'ItemsController@delete')->name('item.delete');

/*
 * Category Routes
 */
Route::get('category', 'CategoryController@index')->name('category.index');
Route::post('category', 'CategoryController@store')->name('category.store');
Route::get('createCategory', 'CategoryController@create')->name('category.create');
Route::get('edit/{id}', 'CategoryController@getCategory')->name('category.getCategory');
Route::post('updateCategory/{id}', 'CategoryController@update')->name('category.update');
Route::get('deleteCategory/{id}', 'CategoryController@delete')->name('category.delete');
