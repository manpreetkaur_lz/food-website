<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Food\Category;
use App\Http\Requests\Backend\CategoryRequest;

/**
 * Description of CategoryController
 *
 * @author pc8
 */
class CategoryController extends Controller
{

    //put your code here
    public function index()
    {
        $categories = Category::get();
        return view('backend.category', compact('categories'));
    }

    public function create()
    {
        return view('backend.create-category');
    }

    public function getCategory($id = null)
    {
        $category = Category::find($id);
        return view('backend.edit-category', compact('category'));
    }

    public function update(CategoryRequest $request, $id = null)
    {
        $category = Category::find($id);
        $category->update($request->all());
        return redirect()->route('admin.category.index')->withFlashSuccess('Category updated Successfully');
    }

    public function delete($id)
    {
        $category = Category::find($id);
        if ($category->delete()) {
            return redirect()->route('admin.category.index')->withFlashSuccess('Category Deleted Successfully');
        }
        return redirect()->route('admin.category.index')->withFlashError('Problem deleting Category');
    }

    public function store(CategoryRequest $request)
    {
        $category = new Category();
        $data     = $category->create($request->all());
        return redirect()->route('admin.category.index')->withFlashSuccess('Category Created Successfully');
    }
}