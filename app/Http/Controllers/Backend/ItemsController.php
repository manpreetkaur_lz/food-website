<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Food\Category;
use App\Models\Food\Item;
use App\Http\Requests\Backend\ItemsRequest;

/**
 * Description of ItemsController
 *
 * @author pc8
 */
class ItemsController extends Controller
{

    public function index()
    {
        $items = Item::get();
        return view('backend.items', compact('items'));
    }

    public function create()
    {
        $categories = $this->getCategories();
        return view('backend.create-item', compact('categories'));
    }

    public function store(ItemsRequest $request)
    {
        $item     = new Item();
        $fileName = '';
        if ($request->hasFile('image')) {
            $directory = storage_path('app/public/uploads');
            $fileName  = date('d-m-y-h-i-s-').preg_replace('/\s+/', '-',
                    trim($request->file('image')->getClientOriginalName()));
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            $request->file('image')->move($directory, $fileName);
        }
        $item->title       = $request->title;
        $item->description = $request->description;
        $item->price       = $request->price;
        $item->featured    = $request->featured;
        $item->category_id = $request->category_id;
        $item->image       = $fileName;
        if ($item->save()) {
            return redirect()->route('admin.items.index')->withFlashSuccess('Item Created Successfully');
        }
        return redirect()->route('admin.items.index')->withFlashError('Problem creating Items');
    }

    public function update(ItemsRequest $request, $id = null)
    {
        if ($id != null) {
            $item     = Item::find($id);
            $fileName = '';
            if ($request->hasFile('image')) {
                $directory = storage_path('app/public/uploads');
                $fileName  = date('d-m-y-h-i-s-').preg_replace('/\s+/', '-',
                        trim($request->file('image')->getClientOriginalName()));
                if (!file_exists($directory)) {
                    mkdir($directory, 0777, true);
                }
                $request->file('image')->move($directory, $fileName);
            }
            $item->title       = $request->title;
            $item->description = $request->description;
            $item->price       = $request->price;
            $item->featured    = $request->featured;
            $item->category_id = $request->category_id;
            if ($fileName != '') {
                $item->image = $fileName;
            }
            if ($item->save()) {
                return redirect()->route('admin.items.index')->withFlashSuccess('Item Edited Successfully');
            }
        }
        return redirect()->route('admin.items.index')->withFlashError('Problem creating Items');
    }

    public function getItem($id = null)
    {
        if ($id != null) {
            $items      = Item::find($id)->toArray();
            $categories = $this->getCategories();
            return view('backend.edit-item', compact('items', 'categories'));
        }
        return redirect()->route('admin.items.index')->withFlashError('Problem creating Items');
    }

    public function delete($id = null)
    {
        if ($id != null) {
            $item = Item::find($id);
            if ($item->delete()) {
                return redirect()->route('admin.items.index')->withFlashSuccess('Item Deleted Successfully');
            }
        }
        return redirect()->route('admin.items.index')->withFlashError('Problem deleting Items');
    }

    private function getCategories()
    {
        $category   = Category::all();
        $categories = [];

        foreach ($category as $cat) {
            $categories[$cat['id']] = $cat['name'];
        }
        
        return $categories;
    }
}