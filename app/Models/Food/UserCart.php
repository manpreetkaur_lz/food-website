<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Food;

use Illuminate\Database\Eloquent\Model;
/**
 * Description of UserCart
 *
 * @author pc8
 */
class UserCart extends Model
{
    //put your code here
    protected $table = 'user_carts';
    protected $fillable = ['user_id','items'];
}