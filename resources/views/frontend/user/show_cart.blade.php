@extends('frontend.layouts.app')
@section('before-styles')
<style>
    tr {
        border-bottom: 1px solid black;
    }
    td{
        padding: 12px !important;
    }
</style>
@endsection

@section('content')
<?php $total = 0;
if (isset($data)) { ?>
    <div class="col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading">Items</div>
            <div class="panel-body">
                <table class = "col-sm-12">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>price</th>
                        </tr>
                    </thead>
                    <?php foreach ($data as $row) {
                        ?>
                        <tr>
                            <td class="col-sm-3">
                                <img src="{{url('/')}}{{ Storage::url('uploads/'.$row['image'])}}" class="img-responsive}}" height="100" width="100"/>
                            </td>
                            <td class="col-sm-3">
                                {{$row['description']}}<br/>
                                <a href="{{ route('frontend.user.items.deleteCart',$row['id']) }}" class="">Delete Cart</a>
                            </td>
                            <td class="col-sm-3">
                                {{$row['category']['name']}}
                            </td>
                            <td class="col-sm-3">
                                {{$row['price']}}
                            </td>
                        </tr>
                        <?php
                        $total   += $row['price'];
                        $items[] = $row['id'];
                    }
                    ?>
                </table>
                <div class="col-sm-3 col-sm-offset-9">
                    <b>Subtotal(<?php count($data); ?> Items) :</b><?php echo $total; ?>
                   <a href="{{ route('frontend.user.items.order',$total) }}" class="btn btn-success">Order</a>
                </div>
            </div>
            </div>
        </div>
<?php } else {?>
<div><h4> Your Sopping Cart is empty.</h4></div>
<?php }?>
@endsection