@extends('backend.layouts.app')
@section('content')
<div class="col-sm-2 col-sm-offset-10" style="padding-bottom: 13px">
    <a href="{{ route('admin.items.create') }}" class="btn btn-success">Add New Item</a>
</div>
<?php 
if (isset($items)) { ?>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Items</div>
            <div class="panel-body">
                 <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php foreach ($items as $row) {
                        ?>
                        <tr>
                            <td class="col-sm-2">
                                <img src="{{url('/')}}{{ Storage::url('uploads/'.$row['image'])}}" class="img-responsive}}" height="100" width="100"/>
                            </td>
                            <td class="col-sm-2">
                                {{$row['title']}}<br/>
                                {{$row['description']}}
                            </td>
                            <td class="col-sm-2">
                                {{$row['category']['name']}}
                            </td>
                            <td class="col-sm-2">
                                {{$row['price']}}
                            </td>
                            <td class="col-sm-2">
                               <a href="{{ route('admin.item.getItem',$row['id']) }}" class="btn btn-success">Edit</a>
                               <a href="{{ route('admin.item.delete',$row['id']) }}" class="btn btn-success">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            </div>
        </div>
<?php }?>
@endsection