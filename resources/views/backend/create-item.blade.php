@extends('backend.layouts.app')
@section('content')
{{ Form::open(['route'=>'admin.items.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','files'=>true]) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Add New Items</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('title', 'Title',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('description', 'Description',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::textarea('description', null, array('class' => 'form-control', 'required')) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('price', 'Price',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::number('price', null, array('class' => 'form-control', 'required')) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('category', 'Category',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                <?php if (!empty($categories)) { ?>
                    {{ Form::select('category_id',$categories , [ 'required' => 'required']) }}
                <?php } else { ?>
                    {{ Form::select('category_id',['null'=>'SELECT'] , ['required' => 'required']) }}
                <?php } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-2">
                {{ Form::label('uploadImage','Upload Image',
                                    ['class' => 'control-label'])}}
            </div>
            <div class="col-xs-8">
                {{ Form::file('image', ['class' => 'field','required' => 'required']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('featured', 'Featured',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::radio('featured', '1',array('class'=>'control-label',['required' => 'required'])) }} Yes<br>
                {{ Form::radio('featured', '0',array('class'=>'control-label',['required' => 'required'])) }} No
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="col-sm-offset-2 col-sm-8">
            {{ Form::submit('Create', ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
        <div class="clearfix"></div>
    </div>
    <!-- /.box-body -->
</div>
@endsection