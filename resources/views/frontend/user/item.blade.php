@extends('frontend.layouts.app')

@section('before-styles')
<style>
    .single-image:nth-child(3n+1)
    {
        clear:both;
    }
    .single-image{
        padding-bottom: 20px;
    }
    .item{
        height: 175px !important;
        width: 300px !important;
    }
    .row{
        padding-bottom: 30px;
    }
</style>
@endsection

@section('content')
{{ Form::open(['route'=>'frontend.user.items.search','class' => 'navbar-form navbar-left', 'role' => 'search', 'method' => 'get']) }}
<div class="input-group">
    <input type="text" id="search" name="search" class="form-control input-xs" placeholder="Seacrh By Title or Category">
    <!--<input type="text" name="search" placeholder="Search by Book Title..." class="form-control">-->
    <span class="input-group-btn"> <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i> </button> </span>
</div> {{ Form::close() }}
<div class="row">
    <a href="{{ route('frontend.user.items.showCart') }}" class="btn btn-info pull-right">Show cart</a>
</div>
<div class="food-items">
    <?php if (!empty($items)) { ?>
        <div class="col-sm-12 items-data">
            <?php
            foreach ($items as $row) {
                ?>
                <div class="col-sm-4 single-image">
                    <b>{{$row['title']}}</b>
                    <img src="{{url('/')}}{{ Storage::url('uploads/'.$row['image'])}}" class="img-responsive item }}"/>
                    <b>Price :{{$row['price']}}</b>
                    <button class="btn btn-success add-cart" type="button" item-id="{{$row['id']}}">Add to cart</button>
                    <!--<button class="btn btn-success show-cart" type="button" item-id="{{$row['id']}}">Show cart</button>-->
                </div>
                <?php
            }
        }else {?>
            <div class="col-sm-12 items-data">
                <h4>No record found, Try to search again !</h4>
            </div>
        <?php }
        ?>
    </div>
</div>
@endsection
@section('after-scripts')
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('.alert-info').fadeOut('fast');
        }, 30000);
    });
    $(document).on("click", ".add-cart", function (e) {
        var $this = $(this);
        if ($this.attr('item-id') != null) {
            $.ajax({
                type: "POST",
                url: "{{ route('frontend.user.items.addCart') }}",
                data: {
                    item_id: $this.attr('item-id'),
                },
                success: function (response) {
                    if (response == 'success') {
                        $this.after('<span class="alert alert-info">Added to the Cart</span>');
                    } else if (response == 'exist') {
                        $this.after('<span class="alert alert-info">Already added to the Cart</span>');
                    }
                },
                error: function (error) {
                    alert(JSON.parse(error));
                }
            });
        }
    });
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection