<?php
/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::get('macros', 'FrontendController@macros')->name('macros');
Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact/send', 'ContactController@send')->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'],
    function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'],
        function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * Food items Routes
         */
        Route::get('items', 'ItemController@index')->name('items.index');
        Route::get('getSearchData', 'ItemController@getItemsOrCategories')->name('items.search');
        Route::post('addCart', 'ItemController@addCart')->name('items.addCart');
        Route::get('deleteCart/{id}', 'ItemController@deleteCart')->name('items.deleteCart');
        Route::get('showCart', 'ItemController@showCart')->name('items.showCart');
        Route::get('order/{price}', 'OrderController@store')->name('items.order');
    });
});
