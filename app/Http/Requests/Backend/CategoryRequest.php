<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

/**
 * Description of CategoryRequest
 *
 * @author pc8
 */
class CategoryRequest extends Request
{

    //put your code here
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}