<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Food\Order;
use App\Models\Food\UserCart;

/**
 * Description of OrderController
 *
 * @author pc8
 */
class OrderController extends Controller
{

    //put your code here
    public function index()
    {

    }

    public function store($price)
    {
        $cart_data = UserCart::where('user_id', access()->id())->first()->toArray();
        $items     = json_decode($cart_data['items'], TRUE);
        if ($price != null) {
            $order               = new Order();
            $order->user_id      = access()->id();
            $order->total        = $price;
            $order->payment_mode = "Cash on delivery";
            if ($order->save()) {
                $order->items()->attach($items['item_id']);
                $user = UserCart::find($cart_data['id']);
                $user->delete();
                return redirect()->route('frontend.user.items.showCart')->withFlashSuccess('Your Items are Successfully Ordered.');
            }
        }
        return response()->json('Error', 500);
    }
}