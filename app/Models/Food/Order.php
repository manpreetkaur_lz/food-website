<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Food;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Order
 *
 * @author pc8
 */
class Order extends Model
{
    //put your code here
    protected $fillable = [];

    public function items()
    {
        return $this->belongsToMany('App\Models\Food\Item','order_items');
    }
}