@extends('backend.layouts.app')
@section('content')
<?php if(isset($category)){?>
{{ Form::open(['method' => 'POST', 'route' => ['admin.category.update', $category['id']],'class' => 'form-horizontal','role'=>'form','files'=>true])}}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Add New Items</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="form-group">
            <div class="col-sm-2 align-right">
                {{ Form::label('name', 'Name',array('class'=>'control-label')) }}
            </div>
            <div class="col-sm-8">
                {{ Form::text('name', $category['name'], array('class' => 'form-control', 'required')) }}
            </div>
        </div>
    <div class="box-body">
        <div class="col-sm-offset-2 col-sm-8">
            {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
        </div>

    </div>
    <!-- /.box-body -->
</div>
{{ Form::close() }}
<div class="clearfix"></div>
<?php } ?>
@endsection