@extends('backend.layouts.app')
@section('content')
<div class="col-sm-2 col-sm-offset-10" style="padding-bottom: 13px">
    <a href="{{ route('admin.category.create') }}" class="btn btn-success">Add New Category</a>
</div>
<?php if (isset($categories)) { ?>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Categories</div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php foreach ($categories as $row) {
                        ?>
                        <tr>
                            <td class="col-sm-5">
                                {{$row['name']}}
                            </td>
                            <td class="col-sm-5">
                                <a href="{{ route('admin.category.getCategory',$row['id']) }}" class="btn btn-success">Edit</a>
                                <a href="{{ route('admin.category.delete',$row['id']) }}" class="btn btn-success">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
<?php } ?>
@endsection